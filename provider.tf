terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.39.0"
    }
  }

  backend "s3" {
    bucket         	   = "tfstatestore1"
    key              	   = "state/terraform.tfstate"
    region         	   = "us-east-1"
    encrypt        	   = true
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}

