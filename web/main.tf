resource "aws_key_pair" "demokey" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key)}"
}


resource "aws_instance" "server" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    subnet_id = var.sn
    security_groups = [var.sg]
    key_name = "${aws_key_pair.demokey.id}"

    tags = {
      Name = "my_server"
    }
}

  
