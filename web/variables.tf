variable "sg" {

  
}

variable "sn" {
  
}

variable "public_key" {
  default = "tests.pub"
}

# Defining Private Key
# variable "private_key" {
#   default = "tests.pem"
# }

# Definign Key Name for connection
variable "key_name" {
  default = "lab4"
  description = "Desired name of AWS key pair"
}